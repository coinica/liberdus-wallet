const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const iv = crypto.randomBytes(16);

export function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

export function setCookie(name, value, path='/') {
  document.cookie = `${name}=${value}; path=${path}`
}

export function deleteCookie(name, path='/') {
  document.cookie = `${name}=; path=${path}`
}

export const encrypt = (text, key) => {
  console.log('encrypt: \n', text, key);
  key = Buffer.from(key, 'hex');
  const cipher = crypto.createCipheriv(algorithm, Buffer.from(key, 'hex'), iv);

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return {
    iv: iv.toString('hex'),
    content: encrypted.toString('hex'),
  };
};

export const decrypt = (hash, key) => {
  const decipher = crypto.createDecipheriv(
    algorithm,
    Buffer.from(key, 'hex'),
    Buffer.from(hash.iv, 'hex')
  );

  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, 'hex')),
    decipher.final(),
  ]);

  return decrpyted.toString();
};

export const getLocalStoreItemWithCookie = (name) => {
  try {
    const storageData = JSON.parse(localStorage.getItem(name))
    const hashKey = getCookie(name)
    console.log(storageData, hashKey)
    const decryptedData = decrypt(storageData, hashKey)
    return JSON.parse(decryptedData)
  } catch(e) {
    console.log(e)
    return {}
  }
}

export const setLocalStoreItemWithCookie = (name, data) => {
  try {
    const hashKey = getCookie(name)
    const encryptedData = encrypt(
      JSON.stringify(data), hashKey
    )
    localStorage.setItem(name, JSON.stringify(encryptedData))
  } catch(e) {
    console.log(e)
  }
}

export const getLocalStoreItem = (name) => {
  // return localStorage.getItem(name)

  try {
    let liberdusInfo = getLocalStoreItemWithCookie('coinica-liberdus')
    console.log('getLocalStoreItem liberdusInfo: \n', liberdusInfo)
    return liberdusInfo[name]
  } catch(e) {
    console.log(e)
    return null
  }
}

export const setLocalStoreItem = (name, value) => {
  // localStorage.setItem(name, value)
  console.log('setLocalStoreItem: \n', name, value)

  try {
    let liberdusInfo = {
      asset: {},
      data: {}
    }

    try {
      liberdusInfo = getLocalStoreItemWithCookie('coinica-liberdus')
    } catch(e) {
      liberdusInfo = {
        asset: {},
        data: {}
      }
    }

    if (!liberdusInfo || !liberdusInfo.asset) {
      liberdusInfo = {
        asset: {},
        data: {}
      }
    }

    console.log('setLocalStoreItem liberdusInfo: \n', liberdusInfo)

    if (name === 'amount') {
      liberdusInfo.asset.amount = {
        ...liberdusInfo.asset.amount,
        [value.alias]: value.data.balance
      }
    } else {
      liberdusInfo[name] = value
    }
    liberdusInfo.asset.updatedAt = Date.now()

    // localStorage.setItem('liberdusInfo', JSON.stringify(liberdusInfo))
    setLocalStoreItemWithCookie('coinica-liberdus', liberdusInfo)
  } catch(e) {
    console.log(e)
  }
}

export const removeLocalStoreItem = (name) => {
  // localStorage.removeItem(name)

  try {
    let liberdusInfo = getLocalStoreItemWithCookie('coinica-liberdus')
    delete liberdusInfo[name]
    liberdusInfo.asset.updatedAt = Date.now()
    // localStorage.setItem('liberdusInfo', liberdusInfo)
    setLocalStoreItemWithCookie('coinica-liberdus', liberdusInfo)
  } catch(e) {
    console.log(e)
  }
}
