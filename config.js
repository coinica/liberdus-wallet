export default {
  server: {
    ip: 'monitor.liberdus.com',
    port: 4000
  },
  proxy: {
    ip: 'test.liberdus.com',
    port: 443
  },
  version: '1.1.0'
}
